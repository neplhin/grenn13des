import React, {Component} from 'react';
import './App.css';
import Header from './components/header/'
import Footer from './components/footer/'
import Body from './components/body/'

class App extends Component {

    render() {
        return (
            <div>
                <WindowDimensions/>
                <Header/>
                <Body/>
                <Footer/>
            </div>

        );
    }



}
class WindowDimensions extends Component {
    constructor(props) {
        super(props);
        this.state = {
            "width": window.innerWidth,
            "height": window.innerHeight,
        };
    }

    render() {
        return(<div></div>);
    }

    updateDimensions() {
        console.log("resize")
        this.setState({"width": window.innerWidth, "height": window.innerHeight});
        if (this.state.width < 800) {
            window.location ='http://mob.a3boots-wordpress-6.tw1.ru/'
        }
    }


    componentDidMount() {
        window.addEventListener("resize", this.updateDimensions.bind(this));
        if (this.state.width < 800) {
            console.log("did mount")
            window.location ='http://mob.a3boots-wordpress-6.tw1.ru/'
        }
    }

    componentWillUnmount() {
        console.log(window.innerWidth);
        console.log(window.innerHeight);
    }
};
export default App;
