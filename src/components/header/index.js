import React, {Component} from 'react';
import './style.css';
import fb from '../../images/fb_5.svg'
import inst from '../../images/Combined Shape.svg'
import logo from '../../images/logo.svg'
import {Container, Nav, Navbar} from "react-bootstrap";
import AnchorLink from 'react-anchor-link-smooth-scroll'

const name = 'Вегетарианська \n кухня';

class Header extends Component {


    render() {
        return (

            <Navbar sticky="top" variant="dark" expand="lg" className="bg">
                <Container>
                    <AnchorLink href="#home"  offset='60'>
                        <Navbar.Brand className="logo-a">
                            <img
                                alt="Green13"
                                src={logo}
                                width="40"
                                height="40"
                                // className="d-inline-block align-top"
                            />
                            <span className="logo-text">Вегетаріанська<br/>кухня</span>
                        </Navbar.Brand>
                    </AnchorLink>
                    <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="justify-content-center mr-auto ml-auto">
                            <AnchorLink href="#about" offset='250' className="nav-link-cust">
                                <Nav.Link style={{color: "#fff"}}>Про нас</Nav.Link>
                            </AnchorLink>
                            <AnchorLink href="#menu" offset='350'  className="nav-link-cust nav-center">
                                <Nav.Link style={{color: "#fff"}}>Меню</Nav.Link>
                            </AnchorLink>
                            <AnchorLink href="#map" offset='1400' className="nav-link-cust">
                                <Nav.Link style={{color: "#fff"}}>Як нас знайти?</Nav.Link>
                            </AnchorLink>
                        </Nav>
                        <Nav>
                            <Nav.Link href="https://www.facebook.com/Green13Cafe/">
                                <img

                                    alt="FB"
                                    src={fb}
                                    width="20"
                                    height="20"
                                    className="d-inline-block align-top"
                                />
                            </Nav.Link>
                            <Nav.Link href="https://www.instagram.com/Green13Cafe/">
                                <img
                                    alt="FB"
                                    src={inst}
                                    width="20"
                                    height="20"
                                    className="d-inline-block align-top"
                                />
                            </Nav.Link>
                            <Nav.Link href="#memes" className="phone">
                                098 294 0231
                            </Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>

        );
    }


}

export default Header;
