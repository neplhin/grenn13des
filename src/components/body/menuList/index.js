import React, {Component} from 'react';
import './style.css';

import MenuItem from "../menuItem"
import {CardDeck} from "react-bootstrap";

import Slider from "react-slick";

class MenuList extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        if (this.props.data.data.length > 10) {
            const settings = {
                dots: false,
                infinite: true,
                speed: 500,
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 3000,

              /*  centerMode: true
                variableWidth: true,
                focusOnSelect: true*/
            };
            return (
                <div className="roll">
                    <span className="menu-title" style={{marginBottom: "45px"}}>{this.props.data.title} </span>
                    <Slider {...settings} >
                        <MenuItem info={this.props.data.data[0]} />
                        <MenuItem info={this.props.data.data[1]} />
                        <MenuItem info={this.props.data.data[2]} />
                        <MenuItem info={this.props.data.data[3]} />
                        <MenuItem info={this.props.data.data[4]} />
                    </Slider>
                </div>

            );
        }
        const elements = this.props.data.data.map((item) =>
            <li key={item.id}>
                <MenuItem info={item}/>
            </li>
        );

        return (
            <div>
                <span className="menu-title">{this.props.data.title}</span>
                <ul style={{listStyleType: "none", paddingLeft: "0", marginTop: "45px"}}>
                    <CardDeck>
                        {elements}
                    </CardDeck>
                </ul>
            </div>
        );
    }
}

export default MenuList;
