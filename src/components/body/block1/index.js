import React, {Component} from 'react';
import { withRouter } from 'react-router-dom';
import './style.css';
import {Button, ButtonToolbar, Col, Container, Row} from "react-bootstrap";
import logoBlock1 from '../../../images/logo_home.png'
import Shape from '../objBlock1/'
import glovo from '../../../images/glovo.svg'
import uber from '../../../images/uber.png'

class Block1 extends Component {

    render() {
        return (
            <div className="bg" id="home">
                <Container>
                    <Row>
                        <Col>
                            <img src={logoBlock1} className="greenPhoto"/>
                            <ButtonToolbar className="buttons-block">
                                <Button variant="warning" className="glovoBtn">
                                    <div>Замовити<img src={glovo} alt="" style={{marginLeft: "75px"}}/></div>
                                </Button>
                                <a href="https://www.ubereats.com/ru-UA/kyiv/food-delivery/green-13-%D0%B1%D0%B5%D1%81%D1%81%D0%B0%D1%80%D0%B0%D0%B1%D1%81%D1%8C%D0%BA%D0%B0-%D0%BF%D0%BB%D0%BE%D1%89%D0%B0/tJZqjBtHQfGUzj8TgSf58A/">
                                    <Button variant="dark" className="uberBtn" >
                                        <div>Замовити<img src={uber} alt="" style={{float: "right"}}/></div>
                                    </Button>
                                </a>
                            </ButtonToolbar>
                        </Col>
                        <Col>
                            <Shape/>
                        </Col>
                    </Row>
                </Container>
            </div>

        );
    }


}

export default Block1;
