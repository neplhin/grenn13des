import React, {Component} from 'react';
import { Card, Col,  Modal, Row} from "react-bootstrap";

import CardOBJ from "../cardOBJ/"
import './style.css';


class MenuItem extends Component {
    constructor(props) {
        super(props);
//        console.log(props)

        this.state = {modalShow: false};
    }

    render() {
        let modalClose = () => this.setState({modalShow: false});

        return (
            <Card style={{width: '348px', height: "548px", border: "none"}}>

                <Card.Img className="card-img-cust"  variant="top" src={this.props.info.img} onClick={() => this.setState({modalShow: true})}/>
                <Card.Body style={{paddingLeft: "unset"}}>
                    <Card.Title className="title grey">{this.props.info.name}</Card.Title>

                    <Card.Text className="text grey">
                        {this.props.info.text}
                    </Card.Text>
                    <Row>
                        <Col lg={8}>
                            <Card.Text className="green text">
                                {this.props.info.info}
                            </Card.Text>
                        </Col>
                        <Col lg={4}>
                            <Card.Text className="weight grey">
                                {this.props.info.weight}
                            </Card.Text>
                            <Card.Text className="price green">
                                {this.props.info.price}
                            </Card.Text>
                        </Col>
                    </Row>
                </Card.Body>

                <MyVerticallyCenteredModal
                    show={this.state.modalShow}
                    onHide={modalClose}
                    obj={this.props.info.obj}
                />
            </Card>


        )
            ;
    }
}

class MyVerticallyCenteredModal extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Modal
                {...this.props}
                size="lg"
                aria-labelledby="contained-modal-title-vcenter"
                centered>
                <Modal.Header closeButton className="">
                </Modal.Header>
                <Modal.Body className="" style={{textAlign: "center"}}>
                    <CardOBJ obj ={this.props.obj}/>
                </Modal.Body>
            </Modal>
        );
    }
}

/*
class App extends React.Component {
    constructor(...args) {
        super(...args);

        this.state = {modalShow: false};
    }

    render() {
        let modalClose = () => this.setState({modalShow: false});

        return (
            <ButtonToolbar>
                <Button
                    variant="primary"
                    onClick={() => this.setState({modalShow: true})}
                >
                    Launch vertically centered modal
                </Button>

                <MyVerticallyCenteredModal
                    show={this.state.modalShow}
                    onHide={modalClose}
                />
            </ButtonToolbar>
        );
    }
}
*/
export default MenuItem;
