import React, {Component} from "react";
import * as THREE from "three";
import {MTLLoader, OBJLoader} from 'mtl-obj-loader'

const OrbitControls = require('three-orbitcontrols');

var objectT;

class CardOBJ extends Component {
    constructor(props) {
        super(props);
        this.animate = this.animate.bind(this);
        this.addCube = this.addCube.bind(this);
        this.initializeCamera = this.initializeCamera.bind(this);
        this.initializeOrbits = this.initializeOrbits.bind(this);
        var path = this.props.obj.path_OBJ;
        var mtl = this.props.obj.name_mtl;
        var obj = this.props.obj.name_obj;
    }

    componentDidMount() {

        const width = 770;
        const height = 570;

        this.scene = new THREE.Scene();
        this.scene.background = new THREE.Color(0xffffff);

        var light3 = new THREE.AmbientLight(0xffffff); // soft white light
        this.scene.add(light3);

        var mtlLoader = new MTLLoader();
        var objLoader = new OBJLoader();

        this.camera = new THREE.PerspectiveCamera(75, width / height, 0.1, 1000);
        this.renderer = new THREE.WebGLRenderer();
        this.controls = new OrbitControls(this.camera, this.renderer.domElement);
        this.controls.enableDamping = true;

        this.renderer.setSize(width, height);
        this.mount.appendChild(this.renderer.domElement);
        this.initializeOrbits();
        this.initializeCamera();

        var scene = this.scene

        {/*  mtlLoader.setTexturePath("/assets/BurgerBacon/");
        mtlLoader.setPath("/assets/BurgerBacon/");
        mtlLoader.load("burgerbacon.mtl", function (materials) { */
        }
        var path = this.props.obj.path_OBJ
        var mtl = this.props.obj.name_mtl;
        var obj = this.props.obj.name_obj;
        mtlLoader.setTexturePath(path);
        mtlLoader.setPath(path);
        mtlLoader.load(mtl, function (materials) {
            materials.preload();
            console.log(materials);
            objLoader.setMaterials(materials);

            objLoader.setPath(path);
            objLoader.load(obj, function (object) {
                var objBbox = new THREE.Box3().setFromObject(object);
                var bboxCenter = objBbox.getCenter().clone();
                bboxCenter.multiplyScalar(-1);

                object.traverse(function (child) {
                    if (child instanceof THREE.Mesh) {
                        child.geometry.translate(bboxCenter.x, bboxCenter.y, bboxCenter.z);
                    }
                });

                objBbox.setFromObject(object);
                console.log(object);
                object.scale.set(.5, .5, .5);
                // objectT = object;
                scene.add(object);

                object.rotation.y += 0.01;


            });

            //  }, 7000);
            console.log(materials);
        });

        setTimeout(() => {
            this.animate();
        }, 0);
    }

    componentWillUnmount() {
        cancelAnimationFrame(this.frameId);
        this.mount.removeChild(this.renderer.domElement);
    }

    initializeOrbits() {
        this.controls.rotateSpeed = .1;
        this.controls.zoomSpeed = 1.2;
        this.controls.panSpeed = 0.8;
    }

    initializeCamera() {
        this.camera.position.x = 15;
        this.camera.position.y = 5;
        this.camera.position.z = 15;
    }

    animate() {
        this.controls.update();
        this.scene.rotation.y += 0.01;

        this.frameId = window.requestAnimationFrame(this.animate);
        this.renderer.render(this.scene, this.camera);

    }

    addCube(cube) {
        this.scene.add(cube);
    }

    render() {
        return (
            <div>
                <div
                    id="boardCanvas"
                    // style={{ width: "80vw", height: "40vw" }}
                    ref={mount => {
                        this.mount = mount;
                    }}
                />
            </div>
        );
    }
}

export default CardOBJ;