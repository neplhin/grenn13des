export default [
    {
        "id": "01",
        "title": "Бургер",
        "data": [
            {
                "id": "1",
                "name": "Бургер Green 13",
                "img": "/img/menuImgs/burger2.png",
                "text": "Зелена булочка із шпинатом, тофу гриль, печені гриби, лист салату, рукола солоний огірок, помідор, маринована червона цибуля, соус песто та тартар.",
                "info": "Білки 16.1 / Жири 20.5 / Вуглеводи 53.9 / Калорійність 464.4",
                "price": "68 грн",
                "weight": "230 г.",
                "obj": {
                    "path_USDZ": "https://www.vectary.com/usdz/embed/952d036d-5b90-465e-af06-3c0342fce5e9",
                    "path_OBJ": "/assets/BurgerGreen/",
                    "name_obj":"burgergreen.obj",
                    "name_mtl":"burgergreen.mtl"
                }
            },
            {
                "id": "2",
                "name": "Бургер вег бекон",
                "img": "/img/menuImgs/burger1.png",
                "text": "Зернова булочка, кисло-солодкий соус, печений солодкий перець, лист салату, помідор, соєвий стейк, бекон із тофу, свіжий огірок, маринована цибуля, майонез.",
                "info": "Білки 16.1 / Жири 20.5 / Вуглеводи 53.9 / Калорійність 464.4",
                "price": "70 грн",
                "weight": "260 г.",
                "obj": {
                    "path_USDZ": "https://www.vectary.com/usdz/embed/30844638-3f3b-403c-9793-0532556b3de0",
                    "path_OBJ": "/assets/BurgerBacon/",
                    "name_obj":"burgerbacon.obj",
                    "name_mtl":"burgerbacon.mtl"
                }
            }
        ]
    },
    {
        "id": "02",
        "title": "Cуп",
        "data": [
            {
                "id": "1",
                "name": "Броколі суп",
                "img": "/img/menuImgs/soup2.png",
                "text": "Броколі, кокосове молоко, зелений горошок, фенхель, цибуля та спеції + грінки, лаваш або тост на вибір.",
                "info": "Білки 16.1 / Жири 20.5 / Вуглеводи 53.9 / Калорійність 464.4",
                "price": "48 грн",
                "weight": "300 г.",
                "obj": {
                    "path_USDZ": "https://www.vectary.com/usdz/embed/980eb254-50fb-4c3a-8bbc-185176f6c87f",
                    "path_OBJ": "",
                    "name_obj":"",
                    "name_mtl":""
                }
            },
            {
                "id": "2",
                "name": "Гарбузовий крем суп",
                "img": "/img/menuImgs/soup1.png",
                "text": "Печений гарбуз, кокосове молоко, кокосова олія, кориця, імбир, гвоздика, цибуля + грінки, лаваш або тост на вибір.",
                "info": "Білки 16.1 / Жири 20.5 / Вуглеводи 53.9 / Калорійність 464.4",
                "price": "46 грн",
                "weight": "300 г.",
                "obj": {
                    "path_USDZ": "https://www.vectary.com/usdz/embed/2a91d56a-03af-4b43-99f4-96b4b700d032",
                    "path_OBJ": "",
                    "name_obj":"",
                    "name_mtl":""
                }
            }
        ]
    },
    {
        "id": "03",
        "title": "Рол",
        "data": [

            {
                "id": "2",
                "name": "Фалафель",
                "img": "/img/menuImgs/falafel.png",
                "text": "Мікс капусти, моркви та зелені, фалафель, помідор, солоний огірок, соус тартар та хумус BBQ.",
                "info": "Білки 16.1 / Жири 20.5 / Вуглеводи 53.9 / Калорійність 464.4",
                "price": "58 грн",
                "weight": "340 г.",
                "obj": {
                    "path_USDZ": "https://www.vectary.com/usdz/embed/30cca60f-16db-4f67-af45-6ff160b28f80",
                    "path_OBJ":"",
                    "name_obj":"",
                    "name_mtl":""
                }
            },
            {
                "id": "3",
                "name": "Рол з гарбузом та соусом ламінарія",
                "img": "/img/menuImgs/rol_garbuz.png",
                "text": "Печений пряний гарбуз, запечений перець, маринована червона капуста, маринована цибуля, кускус, соус ламінарія, пряне насіння соняшника, майонез BBQ, шпинат та свіжий огірок.",
                "info": "Білки 16.1 / Жири 20.5 / Вуглеводи 53.9 / Калорійність 464.4",
                "price": "68 грн",
                "weight": "330 г.",
                "obj": {
                    "path_USDZ": "https://www.vectary.com/usdz/embed/cdedee98-bba4-4f46-bf42-d058076fce1c",
                    "path_OBJ": "/assets/RollPumpkin/",
                    "name_obj":"rollpumpkin.obj",
                    "name_mtl":"rollpumpkin.mtl"
                }
            },
            {
                "id": "1",
                "name": "НеШаурма",
                "img": "/img/menuImgs/ne_sh.png",
                "text": "Лаваш, капуста пекінська, огірок свіжий, помідор свіжий, соус тофу-м'ята, салат пряний з моркви, селери, дайкону, цибуля червона карамелізована, сейтан каррі.",
                "info": "Білки 16.1 / Жири 20.5 / Вуглеводи 53.9 / Калорійність 464.4",
                "price": "60 грн",
                "weight": "280 г.",
                "obj": {
                    "path_USDZ": "https://www.vectary.com/usdz/embed/5922c483-c108-4cf0-9521-36b8e7126181",
                    "path_OBJ":"",
                    "name_obj":"",
                    "name_mtl":""
                }
            },
            {
                "id": "4",
                "name": "Фалафель з зеленого горошку",
                "img": "/img/menuImgs/garbuz.png",
                "text": "Шпинат, айсберг, фалафель, свіжий помідор та огірок, солодка кукурудза, томатний соус та соус тартар.",
                "info": "Білки 16.1 / Жири 20.5 / Вуглеводи 53.9 / Калорійність 464.4",
                "price": "72 грн",
                "weight": "330 г.",
                "obj": {
                    "path_USDZ": "https://www.vectary.com/usdz/embed/144c59b7-1876-412e-9811-0d8777813eda",
                    "path_OBJ": "/assets/RollCorn/",
                    "name_obj":"rollcorn.obj",
                    "name_mtl":"rollcorn.mtl"
                }
            },
            {
                "id": "5",
                "name": "Рол з тофу гриль",
                "img": "/img/menuImgs/tofu.png",
                "text": "Соус тартар, тофу гриль, мікс капусти, моркви та зелені, свіжий помідор та огірок, гімалайська сіль.",
                "info": "Білки 16.1 / Жири 20.5 / Вуглеводи 53.9 / Калорійність 464.4",
                "price": "64 грн",
                "weight": "330 г.",
                "obj": {
                    "path_USDZ": "https://www.vectary.com/usdz/embed/222f0352-46c4-4fe2-b296-039df89e5381",
                    "path_OBJ": "/assets/RollTofu/",
                    "name_obj":"rolltofu.obj",
                    "name_mtl":"rolltofu.mtl"
                }
            }
        ]
    },
    {
        "id": "04",
        "title": "Страви на тарілці",
        "data": [
            {
                "id": "1",
                "name": "Хумус",
                "img": "/img/menuImgs/plate2.png",
                "text": "Хумус, тхіна, паприка, палички з огірка, моркви та селери + лаваш або тост на вибір.",
                "info": "Білки 16.1 / Жири 20.5 / Вуглеводи 53.9 / Калорійність 464.4",
                "price": "52 грн",
                "weight": "270 г.",
                "obj": {
                    "path_USDZ": "https://www.vectary.com/usdz/embed/d8a2be72-afe0-4713-8339-959982271620",
                    "path_OBJ": "/assets/Humus/",
                    "name_obj":"humus.obj",
                    "name_mtl":"humus.mtl"
                }
            },
            {
                "id": "2",
                "name": "Buddha Bowl",
                "img": "/img/menuImgs/plate1.png",
                "text": "Фалафель, соєві стейки, норі, стручкова квасоля, помідор, кукурудза, шпинат, хумус, пекінська капуста, гостро-солодкий, горіховий та кисло-солодкий соус + лаваш або тост на вибір.",
                "info": "Білки 16.1 / Жири 20.5 / Вуглеводи 53.9 / Калорійність 464.4",
                "price": "78 грн",
                "weight": "420 г.",
                "obj": {
                    "path_USDZ": "https://www.vectary.com/usdz/embed/ed5c837c-2806-4f44-94d7-6e01656cb46d",
                    "path_OBJ": "/assets/HumusBoll/",
                    "name_obj":"HumusBoll.obj",
                    "name_mtl":"HumusBoll.mtl"
                }
            }
        ]
    },
    {
        "id": "05",
        "title": "Десерт",
        "data": [
            {
                "id": "1",
                "name": "Чізкейк",
                "img": "/img/menuImgs/cheeseCake.png",
                "text": "Тофу, ваніль, цитрус, печиво, тростниковий цукор + малиновий та шоколадний топінг.",
                "info": "Білки 16.1 / Жири 20.5 / Вуглеводи 53.9 / Калорійність 464.4",
                "price": "45 грн",
                "weight": "140 г.",
                "obj": {
                    "path_USDZ": "https://www.vectary.com/usdz/embed/530fb807-e133-4f32-ab19-c85db1be7196",
                    "path_OBJ":"",
                    "name_obj":"",
                    "name_mtl":""
                }
            }
        ]
    },
    {
        "id": "06",
        "title": "Додатки",
        "data": [
            {
                "id": "1",
                "name": "Додатки",
                "info": [
                    {
                        "name": "Фалафель",
                        "price": "12 грн"
                    },
                    {
                        "name": "Тофу гриль",
                        "price": "12 грн"
                    },
                    {
                        "name": "Бекон із тофу",
                        "price": "10 грн"
                    },
                    {
                        "name": "Соєвий стейк",
                        "price": "12 грн"
                    },
                    {
                        "name": "Печені баклажани",
                        "price": "10 грн"
                    },
                    {
                        "name": "Стручкова квасоля",
                        "price": "8 грн"
                    },
                    {
                        "name": "Печений перець",
                        "price": "10 грн"
                    },
                    {
                        "name": "Печені гриби",
                        "price": "8 грн"
                    },
                    {
                        "name": "Хіяши вакаме",
                        "price": "10 грн"
                    },
                    {
                        "name": "Кукурудза",
                        "price": "8 грн"
                    },
                    {
                        "name": "Помідор",
                        "price": "8 грн"
                    },
                    {
                        "name": "Свіжий огірок",
                        "price": "7 грн"
                    },
                    {
                        "name": "Листя салату",
                        "price": "9 грн"
                    },
                    {
                        "name": "Шпинат",
                        "price": "8 грн"
                    },
                    {
                        "name": "Рукола",
                        "price": "8 грн"
                    }
                    ,
                    {
                        "name": "Кускус",
                        "price": "8 грн"
                    }
                    ,
                    {
                        "name": "Лаваш",
                        "price": "6 грн"
                    }
                    ,
                    {
                        "name": "Тост",
                        "price": "4 грн"
                    }
                ]
            },
            {
                "id": "2",
                "name": "Соуси",
                "info": [
                    {
                        "name": "Чилі",
                        "price": "4 грн"
                    },
                    {
                        "name": "Тхіна",
                        "price": "8 грн"
                    },
                    {
                        "name": "Песто",
                        "price": "13 грн"
                    },
                    {
                        "name": "Хумус",
                        "price": "7 грн"
                    },
                    {
                        "name": "Тартар",
                        "price": "7 грн"
                    },
                    {
                        "name": "Сирний",
                        "price": "16 грн"
                    },
                    {
                        "name": "Горіховий",
                        "price": "8 грн"
                    },
                    {
                        "name": "Хумус BBQ",
                        "price": "7 грн"
                    },
                    {
                        "name": "Кисло-солодкий",
                        "price": "7 грн"
                    },
                ]
            },
            {
                "id": "3",
                "name": "Кава",
                "info": [
                    {
                        "name": "Еспрессо",
                        "price": "18 грн"
                    },
                    {
                        "name": "Американо",
                        "price": "18 грн"
                    },
                    {
                        "name": "Капуоранж",
                        "price": "45 грн"
                    },
                    {
                        "name": "Лате",
                        "price": "34 грн"
                    },
                    {
                        "name": "Капучіно",
                        "price": "26 грн"
                    },
                    {
                        "name": "Флет уайт",
                        "price": "34 грн"
                    },
                    {
                        "name": "Мигдалеве молоко",
                        "price": "6 грн"
                    }
                ]
            },
            {
                "id": "4",
                "name": "Чай",
                "info": [
                    {
                        "name": "Зелений, чорний, трав'яний, каркаде",
                        "price": "16 грн"
                    },
                    {
                        "name": "Обліпиха з тім'яном",
                        "price": "30 грн"
                    },
                    {
                        "name": "Вітамінний чай",
                        "price": "35 грн"
                    },
                    {
                        "name": "Смородина з розмарином",
                        "price": "30 грн"
                    }
                ]
            },
            {
                "id": "5",
                "name": "Напої з молоком",
                "info": [
                    {
                        "name": "Какао",
                        "price": "26 грн"
                    },
                    {
                        "name": "Цикорій",
                        "price": "12 / 26  грн"
                    }
                ]
            },
            {
                "id": "6",
                "name": "Фреш",
                "info": [
                    {
                        "name": "Apple",
                        "price": "20 грн"
                    },
                    {
                        "name": "Orange",
                        "price": "20 грн"
                    },
                    {
                        "name": "Grapefruit",
                        "price": "20 грн"
                    }
                ]
            }
        ]
    }
]