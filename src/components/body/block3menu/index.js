import React, {Component} from 'react';
import './style.css';
import Slider from "react-slick";
import {Button, ButtonGroup, Container} from "react-bootstrap";
import dots from "../../../images/ico_menu.png"
import data from "./menu"
import MenuList from "../menuList"

import Dodatki from "./dodatki"

class Block3 extends Component {

    constructor(props) {
        super(props);
        this.state = {activeIndex: null};
        this.css = {classN: "menu-btn"}
    }

    handleClick = (index) => this.setState({activeIndex: index})

    render() {
        var settings = {
            dots: false,
            infinite: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            vertical: true,
            verticalSwiping: true,
            arrows: false,
            nextArrow: false,
            prevArrow: false,
            adaptiveHeight: true,
            centerMode: true
        };

        return (
            <div style={{marginTop: "50px"}}>
                {/* <ButtonGroup vertical className="btn-group-cust">
                    <Button index={0} variant="outline-dark" className="menu-btn-menu">Меню<img
                        src={dots} alt="" className="btn-img"/></Button>
                    <Button index={1} variant="dark" className={this.css.classN}
                            onClick={() => this.slider.slickGoTo(0)}>Бургери</Button>
                    <Button index={2} variant="dark" className="menu-btn"
                            onClick={() => this.slider.slickGoTo(1)}>Супи</Button>
                    <Button index={3} variant="dark" className="menu-btn"
                            onClick={() => this.slider.slickGoTo(2)}>Роли</Button>
                    <Button index={4} variant="dark" className="menu-btn"
                            onClick={() => this.slider.slickGoTo(3)}>Страви на тарілці</Button>
                    <Button index={5} variant="dark" className="menu-btn"
                            onClick={() => this.slider.slickGoTo(4)}>Десерти</Button>
                    <Button index={6} variant="dark" className="menu-btn" onClick={() => this.slider.slickGoTo(5)}>Додатки
                        та
                        соуси</Button>

                    <Button index={7} variant="dark" className="menu-btn" onClick={() => this.slider.slickGoTo(5)}>Кава</Button>
                    <Button index={8} variant="dark" className="menu-btn" onClick={() => this.slider.slickGoTo(5)}>Чай</Button>
                    <Button index={9} variant="dark" className="menu-btn" onClick={() => this.slider.slickGoTo(5)}>Фреш</Button>
                    <Button index={10} variant="dark" className="menu-btn" onClick={() => this.slider.slickGoTo(5)}>Напої з молоком</Button>
                </ButtonGroup> */}
                <Container id="menu">
                    <div>
                        <h1 className="h1-menu">Меню</h1>
                        <MenuList data={data[0]}/>
                        <MenuList data={data[1]}/>
                        <MenuList data={data[2]}/>
                        <MenuList data={data[3]}/>
                        <MenuList data={data[4]}/>
                        <Dodatki data={data[5]}/>
                        {/*  <Slider {...settings} ref={slider => (this.slider = slider)}>
                            <div>
                                <MenuList data={data[0]}/>
                            </div>
                            <div>
                                <MenuList data={data[1]}/>
                            </div>
                            <div>
                                <MenuList data={data[2]}/>
                            </div>
                            <div>
                                <MenuList data={data[3]}/>
                            </div>
                            <div>
                                <MenuList data={data[4]}/>
                            </div>
                            <div style={{height:"850px"}}>
                                <Dodatki data={data[5]}/>
                            </div>

                        </Slider>*/}
                    </div>
                </Container>
            </div>
        );
    }
}


export default Block3;
