import React, {Component} from 'react';
import './style.css';
import icon from '../../../images/logo_map.svg'
import {Map, GoogleApiWrapper, Marker} from 'google-maps-react';

const mapStyles = {
    width: '100%',
    height: '605px'
};

class BlockMap extends Component {

    render() {
        return (
            <div id="map" style={{marginTop:"50px"}}>
                <h1 className="h1-map">Як нас знайти?</h1>
                <Map
                    google={this.props.google}
                    zoom={14}
                    style={mapStyles}
                    initialCenter={{
                        lat: 50.442372,
                        lng: 30.521609
                    }}
                >
                    <Marker
                        position={{lat: 50.442402, lng: 30.521748}}
                        icon={icon}/>
                    <Marker
                        position={{lat: 50.444997, lng: 30.505643}}
                        icon={icon}/>

                </Map>

            </div>
        );
    }


}

export default GoogleApiWrapper({
    apiKey: 'AIzaSyCZo2xjHHDvLyZIuocbb1ohVXw6zv75Xzc'
})(BlockMap);
