import React, {Component} from 'react';
import Block1 from './block1'
import Block2 from './block2'
import Block3 from './block3menu'
import BlockMap from './blockMap'

class Body extends Component {

    render() {
        return (
            <div>
                <Block1/>
                <Block2/>
                <Block3/>
                <BlockMap/>
            </div>

        )
    }


}

export default Body;
