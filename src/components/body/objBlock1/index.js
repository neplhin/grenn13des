import React, {Component} from "react";
import * as THREE from "three";
import GLTFLoader from 'three-gltf-loader';
import {MTLLoader, OBJLoader} from 'mtl-obj-loader'

const OrbitControls = require('three-orbitcontrols');


var loadingScreen = {
    scene: new THREE.Scene(),
    camera: new THREE.PerspectiveCamera(75, 500 / 500, 0.1, 1000),
    loader: new GLTFLoader()
}


var RESOURCES_LOADED = false;

class Shape extends Component {
    constructor(props) {
        super(props);
        this.animate = this.animate.bind(this);
        this.addCube = this.addCube.bind(this);
        this.initializeCamera = this.initializeCamera.bind(this);
        this.initializeOrbits = this.initializeOrbits.bind(this);
    }

    componentDidMount() {
        const width = 500;
        const height = 500;


        loadingScreen.loader.load(
            '/assets/loading_animation/scene_new.gltf',
            (gltf) => {
                // called when the resource is loaded
              //  gltf.position.set(0, 0, 0);
                console.log("========")
                console.log(gltf)
               gltf.scene.scale.set(0.1, 0.1, 1);
               gltf.scene.position.set(-0.15, 0, 0);
                console.log("========")
                loadingScreen.scene.add(gltf.scene);
             //   loadingScreen.camera.lookAt(gltf.scene);

            },
            (xhr) => {
                // called while loading is progressing
                console.log(`${(xhr.loaded / xhr.total * 100)}% loaded`);
            },
            (error) => {
                // called when loading has errors
                console.error('An error happened', error);
            },
        );
        // loadingScreen.scene.add( gltf.scene);


        loadingScreen.scene.background = new THREE.Color(0x242323);

        var loadingManager = new THREE.LoadingManager();
        loadingManager.onProgress = function (item, loaded, total) {
            console.log(item, loaded, total);
        };
        loadingManager.onLoad = function () {
            console.log("LOADED ALL RESOURCES");
            RESOURCES_LOADED = true;
        };


        this.scene = new THREE.Scene();
        this.scene.background = new THREE.Color(0x242323);

        var light3 = new THREE.AmbientLight(0xffffff); // soft white light
        this.scene.add(light3);

        var mtlLoader = new MTLLoader(loadingManager);
        var objLoader = new OBJLoader(loadingManager);

        this.camera = new THREE.PerspectiveCamera(75, width / height, 0.1, 1000);
        this.renderer = new THREE.WebGLRenderer();
        this.controls = new OrbitControls(this.camera, this.renderer.domElement);
        this.controls.enableDamping = true;

        this.renderer.setSize(width, height);
        this.mount.appendChild(this.renderer.domElement);
        this.initializeOrbits();
        this.initializeCamera();


        var scene = this.scene;

        mtlLoader.setTexturePath("/assets/BurgerGreen/");
        mtlLoader.setPath("/assets/BurgerGreen/");
        mtlLoader.load("burgergreen.mtl", function (materials) {

            materials.preload();
            console.log(materials);
            objLoader.setMaterials(materials);
            objLoader.setPath('/assets/BurgerGreen/');
            objLoader.load("burgergreen.obj", function (object) {
                var objBbox = new THREE.Box3().setFromObject(object);
                var bboxCenter = objBbox.getCenter().clone();
                bboxCenter.multiplyScalar(-1);

                object.traverse(function (child) {
                    if (child instanceof THREE.Mesh) {
                        child.geometry.translate(bboxCenter.x, bboxCenter.y, bboxCenter.z);
                    }
                });

                objBbox.setFromObject(object);
                object.scale.set(0.5, 0.5, 0.5);
                scene.add(object);
            });

        });

        setTimeout(() => {
            this.animate();
        }, 0);
    }

    componentWillUnmount() {
        cancelAnimationFrame(this.frameId);
        this.mount.removeChild(this.renderer.domElement);
    }

    initializeOrbits() {
        this.controls.rotateSpeed = .1;
        this.controls.zoomSpeed = 1.2;
        this.controls.panSpeed = 0.8;
    }

    initializeCamera() {
        this.camera.position.x = 17;
        this.camera.position.y = 7;
        this.camera.position.z = 17;
    }

    animate() {
        if (RESOURCES_LOADED === false) {
            requestAnimationFrame(this.animate);


            this.renderer.render(loadingScreen.scene, loadingScreen.camera);
            return;
        }


        this.controls.update();
        this.scene.rotation.y += 0.001;

        this.frameId = window.requestAnimationFrame(this.animate);
        this.renderer.render(this.scene, this.camera);

    }

    addCube(cube) {
        this.scene.add(cube);
    }

    render() {
        return (
            <div>
                <div
                    id="boardCanvas"
                    ref={mount => {
                        this.mount = mount;
                    }}
                />
            </div>
        );
    }
}

export default Shape;