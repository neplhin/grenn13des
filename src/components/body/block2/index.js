import React, {Component} from 'react';
import './style.css';
import {Col, Container, Row} from "react-bootstrap";
import foto from '../../../images/foto.png'


class Block1 extends Component {

    render() {
        return (
            <div id="about">
                <Container className="container-cust">
                    <Row>
                        <Col>
                            <h1 className="h1-cust">Про нас</h1>
                            <p className="p-cust">
                                Green 13 Cafe Vegan Kitchen
                                ми стали піонерами Української веган-стріт-культури та працюємо для Вас щодня вже третій рік
                            </p>
                        </Col>
                        <Col>
                            <img src={foto} />
                        </Col>
                    </Row>
                </Container>
            </div>

        );
    }


}

export default Block1;
