import React, {Component} from 'react';
import './style.css';
import {Button, Col, Container, Row} from "react-bootstrap";
import logoFooter from '../../images/logo_footer.svg'
import fb from '../../images/fb_5.svg'
import inst from '../../images/Combined Shape.svg'
import logoAmeal from '../../images/logo-ameal.svg'
import glovo from '../../images/glovo.svg'
import uber from '../../images/uber.png'

class Footer extends Component {

    render() {
        return (
            <div className="bg-footer">
                <Container>
                    <Row>
                        <Col>
                            <img src={logoFooter} alt=""/>
                        </Col>
                        <Col>
                            <p className="footer-text">Бессарабська площа, 2<br/>
                                "Бессарабський ринок "</p>
                            <p className="footer-text">10:00 - 22:00</p>
                        </Col>
                        <Col style={{display: "flex"}}>
                            <div>
                                <p className="footer-text">Тараса<br/>
                                    Шевченка 26/4</p>
                                <p className="footer-text">10:00 - 22:00</p>
                            </div>
                            <div className="social-block">
                                <a href="https://www.facebook.com/Green13Cafe/"><img src={fb} className="social-img" alt="" width={30} height={30}/></a>
                                <a href="https://www.instagram.com/Green13Cafe/"><img src={inst} alt="" width={30} height={30}/></a>
                            </div>
                        </Col>
                        <Col style={{position: "relative", bottom: "20px"}}>
                            <Button variant="warning" className="glovoBtn-footer">
                                <div><span>Замовити</span><img src={glovo} alt="" style={{marginLeft: "75px"}}/></div>
                            </Button>

                            <a href="https://www.ubereats.com/ru-UA/kyiv/food-delivery/green-13-%D0%B1%D0%B5%D1%81%D1%81%D0%B0%D1%80%D0%B0%D0%B1%D1%81%D1%8C%D0%BA%D0%B0-%D0%BF%D0%BB%D0%BE%D1%89%D0%B0/tJZqjBtHQfGUzj8TgSf58A/">
                                <Button
                                    variant="dark" className="uberBtn-footer">
                                    <div><span>Замовити</span><img src={uber} alt="" style={{float: "right"}}/></div>
                                </Button>
                            </a>
                        </Col>
                    </Row>
                    <Row style={{paddingTop: "40px"}}>
                        <Col className="terms">
                            <p className="terms-otstup">Privacy policy</p>
                            <p>Terms of Use</p>
                        </Col>
                        <Col className="copyright">
                            <p className="green13-c">Green 13 2019 ©</p>
                        </Col>
                        <Col>
                            <a href="https://ameal.me/">
                                <img src={logoAmeal} alt="" style={{float: "right"}}/>
                            </a>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }


}

export default Footer;
